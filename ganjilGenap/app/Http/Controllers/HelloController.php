<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\Foreach_;

class HelloController extends Controller
{
    public function hello(){
        $hello = "Hello";
        $world = "world";

        return view('welcome')->with([
            'hello'=>$hello,
            'world'=>$world,
        ]);
    } 
    public function ganjilGenap(Request $request){
        $validator = Validator::make($request->all(), [
            'angka1' => 'required',
            'angka2' => 'required',
        ]);
        $b = $request->angka1;
        $c = $request->angka2;      
        for ($i=$b; $i <=$c ; $i++) { 
            if($i % 2 ==0){
                echo '<table border="1"><tr><td>Angka ' .$i.' adalah genap'.'<td></tr></table>';
            }else{
                echo '<table border="1"><tr><td>Angka ' .$i.' adalah ganjil'.'<td></tr></table>';
            }
        }

    } 
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HelloController extends Controller
{
   public function hello(){
       $hello = "Hello";
       $world = "world";

       return view('welcome')->with([
           'hello'=>$hello,
           'world'=>$world,
       ]);
   } 
}
